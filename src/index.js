require('dotenv').config();

const express = require('express');
const app = express();
const cors = require('cors');
const http = require('http');
const server = http.createServer(app);

const socketUtil = require('./util/socketUtil');

socketUtil.init(server);
const corsOptions = {
    origin: "http://localhost" 
}
app.use(cors(corsOptions));

const userService = require('./service/userService');
const chatService = require('./service/chatService');
const gameService = require('./service/gameService');
const notificationService = require("./service/notificationService");

const user = require('./controller/userController');
const web = require('./controller/webController');
app.use(user);
app.use(web);

server.listen(process.env.port, () => {
    console.log(`listening on *:${process.env.port}`);
});
const WS = {
  CONNECTION: "connection",
  DISCONNECT: "disconnect",
  DISCONNECTING: "disconnecting",
  LOGIN: "login",
  LOGOUT: "logout",
  NEW_CONNECTED_USER: "newConnectedUser",
  USER_DISCONNECTED: "userDisconnected"
}

const CHAT = {
  MESSAGE: "message"
}

const GAME = {
  JOIN_WAITING_LIST: "joinWaitingList",
  QUIT_WAITING_LIST: "quitWaitingList",
  JOINED: "joined",
  ACTION: "action",
  GAME_UPDATED: "gameUpdated",
  END_TURN: "endTurn",
  ATTACK: "attack"
}

module.exports = {
  CHAT,
  WS,
  GAME
}
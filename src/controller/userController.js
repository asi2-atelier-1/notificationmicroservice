const express = require('express');
const userService = require("../service/userService");
const router = express.Router();

router.use(express.static('public'));
router.get('/users', (req, res) => {
    res.send(userService.getConnectedUsers());
});

module.exports = router;
const socketUtil = require('../util/socketUtil'); 
const esbUtil = require('../util/esbUtil');

const sendNotification = (destination, notificationAction, message) => {
    socketUtil.publish(destination, JSON.stringify({notificationAction, message}));
} 

const onNotificationReceived = (body) => {
    sendNotification(body.userId, body.nA, body.message);
}

esbUtil.subscribe('notification', onNotificationReceived);

module.exports = {
    sendNotification
};
const socketUtil = require('../util/socketUtil'); 
const { CHAT, WS } = require("../consts/endPoints");

const users = {};

const connection = (socket) => {
	let userId = null;

	const login = userInfo => {
		userId = userInfo.id;

		users[socket.id] = {
			...userInfo,
			socketId: socket.id
		};
		socketUtil.io.emit(WS.NEW_CONNECTED_USER, users[socket.id]);
		console.log(`⚡: ${socket.id} user just connected!`);
	}

	const disconnect = () => {
		socketUtil.io.emit(WS.USER_DISCONNECTED, users[socket.id]);
		delete users[socket.id];
		console.log(`🔥: A user disconnected`);
	}
	
	socket.on(WS.LOGIN, login);
	socket.on(WS.DISCONNECT, disconnect);
	socket.on(WS.LOGOUT, disconnect);
}

socketUtil.subscribe(WS.CONNECTION, connection);

module.exports = {
	getConnectedUsers: () => Object.values(users),
  users
}
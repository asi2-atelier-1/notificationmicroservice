const socketUtil = require('../util/socketUtil'); 
const { post } = require('../util/esbUtil')
const { CHAT, WS } = require("../consts/endPoints");
const { users } = require('./userService');

const connection = (socket) => {	
	const message = (message) => {
		const newMessage = {
			...message,
			source: users[socket.id],
			destination: users[message.destination]
		};

		if(newMessage.source === undefined)
			return;

		if(newMessage.destination === undefined) {
			socketUtil.publish(CHAT.MESSAGE, newMessage);
		} else {
			socketUtil.io.to(newMessage.source.socketId)
				.emit(CHAT.MESSAGE, newMessage);
			socketUtil.io.to(newMessage.destination.socketId)
				.emit(CHAT.MESSAGE, newMessage);
		}

		post('ChatHistoryQueue', {
			srcId: newMessage.source.id,
			destId: newMessage.destination.id,
			text: newMessage.text,
			time: new Date()
		});
	}

	socket.on(CHAT.MESSAGE, message);
}

socketUtil.subscribe(WS.CONNECTION, connection);
const socketUtil = require('../util/socketUtil');
const { GAME, WS } = require("../consts/endPoints");
const { ALERT } = require("../consts/notifications");
const { users } = require('./userService');
const { sendNotification } = require('./notificationService');
const STATUS = require('../consts/gameStatus');

const MAX_ENERGY = 200;

const generateRandomId = () => Math.random().toString(36).substring(2, 13);

let waitingList = [];
const rooms = {};

const isUserInWaitingList = (user) => waitingList.some(wu => wu.id === user.id);
const isUserInRooms = (user) => Object.values(rooms).some(room => Object.values(room.users).some(u => u.id === user.id));
const findRoom = (user) => Object.values(rooms).find(room => Object.values(room.users).some(u => u.id === user.id));

const connection = (socket) => {
  const joinWaitingList = ({ cards }) => {
    const newUser = {
      ...users[socket.id],
      cards
    };

    // add user to waiting list if no users in list
    if (waitingList.length === 0) {
      waitingList.push(newUser);
      console.log(`✋: ${socket.id} user waiting!`);
      return;
    }

    if (isUserInWaitingList(newUser) || isUserInRooms(newUser)) {
      return;
    }

    // get other waiting user
    const otherUser = waitingList.pop();

    // turn order
    const chance = Math.random();
    if (chance < 0.5) {
      otherUser.actionPoint = MAX_ENERGY;
      newUser.actionPoint = 0;
    } else {
      otherUser.actionPoint = 0;
      newUser.actionPoint = MAX_ENERGY;
    }

    const room = {
      id: generateRandomId(),
      status: STATUS.READY,
      users: {
        [newUser.id]: newUser,
        [otherUser.id]: otherUser
      },
      turnUser: newUser.actionPoint > 0 ? newUser.id : otherUser.id
    }

    // save room in memory
    rooms[room.id] = room;

    // users join the room
    socket.join(room.id);
    socketUtil.io.sockets.sockets.get(otherUser.socketId).join(room.id);

    // emit users joined the room
    socketUtil.io.to(room.id).emit(GAME.JOINED, room);

    console.log(`💪: room created!`);
  }

  const quitWaitingList = () => {
    waitingList = waitingList.filter(wu => wu.socketId === socket.id);
  }

  const disconnecting = () => {
    socket.rooms.forEach(roomId => {
      delete rooms[roomId];
    });
  }

  const attack = ({ idCardSource, idCardTarget }) => {
    const user = users[socket.id];
    const room = findRoom(user);

    if(room === undefined || room.turnUser !== user.id) {
      sendNotification(user.id, ALERT, "La partie n'est plus valide !");
      return;
    }
    
    const otherUser = Object.values(room.users).find(u => u.id !== user.id);
    const cardSource = room.users[user.id].cards[idCardSource];
    const cardTarget = otherUser.cards[idCardTarget];

    console.log(cardSource, cardTarget)
    if(cardSource === undefined || cardTarget === undefined) {
      sendNotification(user.id, ALERT, "Veuillez sélectioner une carte pour attaquer et à attaquer !");
      console.error("CardSource/Target undefined", cardSource, cardTarget)
      return;
    }
    
    if(room.users[user.id].actionPoint - cardSource.cardInstance.energy < 0) {
      sendNotification(user.id, ALERT, "Vous n'avez pas assez d'energie pour utiliser cette carte !");
      console.error("Not enough energy to play this card")
      return;
    }
    
    cardTarget.cardInstance.hp = Math.max(0, cardTarget.cardInstance.hp - cardSource.cardInstance.attack);

    if(cardTarget.cardInstance.hp === 0) {
      console.log("CardInstance " + cardTarget.cardInstance.idCardInstance + " is dead")
      sendNotification(user.id, ALERT, "Une carte a été détruite !");
      delete room.users[otherUser.id].cards[cardTarget.cardInstance.idCardInstance];
    }

    
    if(Object.values(room.users[otherUser.id].cards).length === 0) {
      console.log("No more cards to play")
      setTimeout(() => {
        room.status = STATUS.ENDED;
        socketUtil.io.to(room.id).emit(GAME.GAME_UPDATED, room);
        delete rooms[room.id];
      }, 500);
    }

    room.users[user.id].actionPoint -= cardSource.cardInstance.energy;

    console.log("UPDATING GAME", room)
    socketUtil.io.to(room.id).emit(GAME.GAME_UPDATED, room);
  }

  const endTurn = () => {
    const user = users[socket.id];
    const room = findRoom(user);

    if(room === undefined || room.turnUser !== user.id)
      return;
    
    room.turnUser = Object.values(room.users).find(u => u.id !== user.id).id;
    room.users[room.turnUser].actionPoint = MAX_ENERGY;
    room.users[user.id].actionPoint = 0;
    socketUtil.io.to(room.id).emit(GAME.GAME_UPDATED, room);
  }

  socket.on(GAME.JOIN_WAITING_LIST, joinWaitingList);
  socket.on(GAME.QUIT_WAITING_LIST, quitWaitingList);
  socket.on(WS.DISCONNECTING, disconnecting);
  socket.on(GAME.ATTACK, attack);
  socket.on(GAME.END_TURN, endTurn);
}

socketUtil.subscribe(WS.CONNECTION, connection);
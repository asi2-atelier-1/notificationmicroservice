const emailInput = document.querySelector("#email");
const passwordInput = document.querySelector("#password");

const loginBtn = document.querySelector(".login100-form-btn");
loginBtn.addEventListener("click", () => {
    console.log(emailInput.value, passwordInput.value)
    login();
});

const login = async (email = "testEmail@test.com", pwd = "testPwd") => {
    const rawResponse = await fetch('http://localhost:8085/auth/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, // this line is important, if this content-type is not set it wont work
        body: `email=${email}&pwd=${pwd}`
    });
    const profil = await rawResponse.json();

    const socket = io();

    socket.on(profil.email, function (notfication) {
        console.log(notfication);
    });
}
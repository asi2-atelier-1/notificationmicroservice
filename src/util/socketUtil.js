const { Server } = require("socket.io");
let io;

module.exports = {
    init: (server) => {
        io = new Server(server, {
            // transports: ['websocket'],
            // allowEIO3: true,
            cors: {
                origin: "http://localhost"
            }
        });

        module.exports.io = io;
    },
    publish: (topic, message) => {
        io.emit(topic, message);
    },
    subscribe: (topic, callback) => {
        io.on(topic, callback);
    },
    io
}

const stompit = require('stompit');

const subscribers = {};

const connectOptions = {
    'host': 'localhost',
    'port': 61613,
    'connectHeaders': {
        'host': '/',
        'login': 'admin',
        'passcode': 'admin',
        'heart-beat': '0,0'
    }
};

const subscribeHeaders = {
    'destination': '/queue/NotificationQueue',
    'ack': 'client-individual'
};

let lastClient;

stompit.connect(connectOptions, (error, client) => {
    if (error) {
        console.log('connect error ' + error.message);
        return;
    }

    client.subscribe(subscribeHeaders, (error, message) => {
        if (error) {
            console.log('subscribe error ' + error.message);
            return;
        }

        message.readString('utf-8', function (error, body) {
            if (error) {
                console.log('read message error ' + error.message);
                return;
            }
            const parsed = JSON.parse(body);
            console.log(`Dequeue message: ${body}`)

            Object.values(subscribers).forEach(callback => {
                callback(parsed);
            })

            client.ack(message);
        });
    });

    lastClient = client;

    // setInterval(() => {
    //     const sendHeaders = {
    //         'destination': '/queue/ChatHistoryQueue',
    //         'content-type': 'text/plain'
    //     };
    //
    //     const frame = client.send(sendHeaders);
    //     frame.write(JSON.stringify({ "srcId": 1,
    //                                  "destId": 1,
    //                                  "text": "Testing",
    //                                  "time": new Date()
    //                                 }));
    //
    //     // frame.write(JSON.stringify({ email: "testEmail@test.com", message: "Bonjour testEmail et bienvenue"}));
    //     frame.end();
    // }, 1000);
});

module.exports = {
    subscribe: (id, callback) => {
        subscribers[id] = callback;
    },
    unsubscribe: (id) => {
        subscribers[id] = null;
    },
    post: (queue, message) => {
        if (lastClient === undefined)
            return;

        const sendHeaders = {
            'destination': '/queue/' + queue,
            'content-type': 'text/plain'
        };

        const frame = lastClient.send(sendHeaders);
        frame.write(JSON.stringify(message));

        frame.end();
    }
}
